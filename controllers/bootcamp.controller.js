/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-08-04T13:07:09+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:07:09+05:30
 */



const Bootcamp = require('../models/bootcamp.model');
const errorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middlewares/async');
const geocoder = require('../utils/geocoder');
const path = require('path');
exports.createBootcamp = async (req, res, next) => {
  console.log(req.body);
  req.body.user = req.user.id;
	try {
    //check for publised boot camp
    const publishedBootcamp = await Bootcamp.findOne({user: req.body.user });
    // if the user is not admin only one additional
    if(publishedBootcamp && req.body.user!='admin'){
      return next(new errorResponse(`user with id ${req.user.id} already published a boot camp`,400));
    }
    const bootcamp = await Bootcamp.create(req.body);

		res.status(201).json({ success: true, data: bootcamp });
	} catch (err) {
    console.log(err)
		res.status(400).json({ success: false });
	}
};

exports.getBootcamps = asyncHandler (async (req, res, next) => {



  res.status(200).json(res.advancedResults);
		// res
		// 	.status(200)
		// 	.json({ success: true,pagination:res.pagination, count: res.advancedResults.length, data: res.advancedResults });

	// res.status(200).json({ success: true, msg: 'show all boot camps conroller' });
});

exports.getBootcamp = asyncHandler(async (req, res, next) => {

		const bootcamp = await Bootcamp.findById(req.params.id);
		if (!bootcamp) {
			const error = new errorResponse(
				`Boot Camp not found with id -> : ${req.params.id}`,
				404
			);
			next(error);
		} else {
			res.status(200).json({ success: true, data: bootcamp });
		}

});

exports.updateBootcamp = async (req, res, next) => {
	try {
		let bootcamp = await Bootcamp.findById(req.params.id);
		if (!bootcamp) {
      const error = new errorResponse(
				`Boot Camp not found with id : ${req.params.id}`,
				404
			);
			next(err);

    }
    if(bootcamp.user.tString()!==req.user.id || req.user.role!==admin )	{
      return next(new errorResponse(
				`user with id : ${req.user.id} ot authorized to do this `,
				403
			));
    }

      bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id,  req.body, {
        new: true,
        runValidators: true,
      });
    res.status(200).json({ success: true, data: bootcamp });
	} catch (err) {
    next(err);
	}
};
exports.deleteBootcamp = async (req, res, next) => {
	try {
		const bootcamp = await Bootcamp.findById(req.params.id);
		if (bootcamp) {
      if(bootcamp.user.tString()!==req.user.id || req.user.role!==admin )	{
        return next(new errorResponse(
          `user with id : ${req.user.id} ot authorized to do this `,
          403
        ));
      }

      bootcamp.remove();
			res.status(200).json({ success: true, data: {} });
		} else {
			const error = new errorResponse(
				`Boot Camp not found with id : ${req.params.id}`,
				404
			);
			next(err);
		}
	} catch (err) {
    next(err);
	}
};

// get boot camps with in a radius

// get api/v1/bootcamps/radius/:zipcode/:distance

exports.getBootcampsInRadius = async (req, res, next) => {

  const { zipcode, distance} = req.params;
  const loc = await geocoder.geocode(zipcode);
  const lat = loc[0].latitude;
  const lng = loc[0].longitude;
  // calc radius using radians
  // divide distance by radius of earth Earth rad: 3963 miles 6378 km
const radius = distance / 3963 ;
const boomCamps = await Bootcamp.find({
  location:{ $geoWithin:{$centerSphere:[[lng, lat], radius]}}
})
res.status(200).json({
success: true,
count :boomCamps.length,
data:boomCamps
});
}


// upload photo put request bootcamps/id/phoo
exports.bootcampPhotoUpload = asyncHandler(async (req, res, next) => {

		const bootcamp = await Bootcamp.findById(req.params.id);
		if (!bootcamp) {
      return next(new errorResponse(`Boot Camp not found with id : ${req.params.id}`,404));
			// res.status(200).json({ success: true, data: {} });
    }
    if(bootcamp.user.tString()!==req.user.id || req.user.role!==admin )	{
      return next(new errorResponse(
        `user with id : ${req.user.id} ot authorized to do this `,
        403
      ));
    }

    if(!req.files){
      return next(new errorResponse( `please upload file`,400))
    }
    // console.log(req.files)
    const file = req.files.file;
    if(!file.mimetype.startsWith('image')){
      return next(new errorResponse( `please upload an image file `,400))
   }
   if(file.size> process.env.MAX_FILE_UPLOAD_SIZE){
    return next(new errorResponse( `please upload an image file less than 1 mb sizeß `,400))

   }
   file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
   file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`,async err=>{
      if(err){
        console.log(err)
        return next(new errorResponse( `Problem with file upload `,400))
      }
      await Bootcamp.findByIdAndUpdate(req.params.id,{photo:file.name})
      res.status(200).json({success:true,data:file.name})
   })

 });
