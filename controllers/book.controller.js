/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-08-04T10:23:17+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T12:59:52+05:30
 */
 const BookModel= require('../models/book.model');
 const errorResponse = require('../utils/errorResponse');
 const asyncHandler = require('../middlewares/async');
 const path = require('path');

/**
*
*
* @param {*} req  HTTP request argument to the middleware function, called "req" by convention.
* @param {*} res  HTTP response argument to the middleware function, called "res" by convention.
* @param {*} net Callback argument to the middleware function, called "next" by convention.
* @returns Promise
* @memberof BookController  Used for  create a new book
*/
 exports.createBook = async (req, res, next) => {
   console.log(req.body);
   // req.body.user = req.user.id;
 	try {

     const book = await BookModel.create(req.body);
 		res.status(201).json({ success: true, data: book });
 	} catch (err) {
     console.log(err)
 		res.status(400).json({ success: false });
 	}
 };

 /**
 *
 *
 * @param {*} req  HTTP request argument to the middleware function, called "req" by convention.
 * @param {*} res  HTTP response argument to the middleware function, called "res" by convention.
 * @param {*} net Callback argument to the middleware function, called "next" by convention.
 * @returns Promise
 * @memberof BookController  Used for  get all books
 */

 exports.getBooks = asyncHandler (async (req, res, next) => {
   res.status(200).json(res.advancedResults);
 });

 /**
 *
 *
 * @param {*} req  HTTP request argument to the middleware function, called "req" by convention.
 * @param {*} res  HTTP response argument to the middleware function, called "res" by convention.
 * @param {*} net Callback argument to the middleware function, called "next" by convention.
 * @returns Promise
 * @memberof BookController  Used for  get a particular book based on ID
 */
 exports.getBook = asyncHandler(async (req, res, next) => {

 		const book = await BookModel.findById(req.params.id);
 		if (!book) {
 			const error = new errorResponse(
 				`Book  not found with id -> : ${req.params.id}`,
 				404
 			);
 			next(error);
 		} else {
 			res.status(200).json({ success: true, data: book });
 		}

 });


 /**
 *
 *
 * @param {*} req  HTTP request argument to the middleware function, called "req" by convention.
 * @param {*} res  HTTP response argument to the middleware function, called "res" by convention.
 * @param {*} net Callback argument to the middleware function, called "next" by convention.
 * @returns Promise
 * @memberof BookController  Used for  updating  book based on the input ID
 */

 exports.updateBook = async (req, res, next) => {
 	try {
 		let book = await BookModel.findById(req.params.id);
 		if (!book) {
       const error = new errorResponse(
 				`Book not found with id : ${req.params.id}`,
 				404
 			);
 			next(err);

     }
     updatedbook = await BookModel.findByIdAndUpdate(req.params.id,  req.body, {
         new: true,
         runValidators: true,
       });
     res.status(200).json({ success: true, data: updatedbook });
 	} catch (err) {
     next(err);
 	}
 };

 /**
 *
 *
 * @param {*} req  HTTP request argument to the middleware function, called "req" by convention.
 * @param {*} res  HTTP response argument to the middleware function, called "res" by convention.
 * @param {*} net Callback argument to the middleware function, called "next" by convention.
 * @returns Promise
 * @memberof BookController  Used for  Delete  a  book based on ID
 */

 exports.deleteBook = async (req, res, next) => {
 	try {
 		const book = await BookModel.findById(req.params.id);
 		if (book) {
      book.remove();
 			res.status(200).json({ success: true, data: {} });
 		} else {
 			const error = new errorResponse(
 				`Book  not found with id : ${req.params.id}`,
 				404
 			);
 			next(error);
 		}
 	} catch (err) {
     next(err);
 	}
 };
