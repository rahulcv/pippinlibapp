/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-08-04T10:23:49+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T10:52:22+05:30
 */
 const mongoose = require('mongoose');
 const slugify = require('slugify');
 const BookSchema = new mongoose.Schema({
   name:{
 type: 'string',
 required:[true,'Please add a book name'],
 unique:true,
 trim:true,
 maxLength:[50,'Book Name cannot be more than 50 char']

   },
   slug:String,
   description:{
     type: 'string',
   required:[true,'Please add a book description'],
   unique:false,
   trim:false,
   maxLength:[500,'desc cannot be more than 500 char']
   },
  author: {
     type: String,
     maxlength: [50, 'Auhor Name cannot be more than 50 char']
   },
   averageRating: {
     type: Number,
     min: [1, 'Rating must be at least 1'],
     max: [10, 'Rating must can not be more than 10'],
     default:5,
   },
   cost: {
     type: Number,
     default:0,
   },
   coverphoto: {
     type: String,
     default: 'no-photo.jpg'
   },
   createdAt: {
     type: Date,
     default: Date.now
   },
 },
  {
     toJSON: { virtuals: true },
     toObject: { virtuals: true }
   });

   // create bootcamp slug from name
   BookSchema.pre('save',function(next){
       console.log('Slugify ran', this.name)
       this.slug = slugify(this.name);
       next();
   });
   // geocode create location field

   module.exports = mongoose.model('Book', BookSchema);
