/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-06-28T17:08:25+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:00:55+05:30
 */



/* FrameWork Packages Start*/
const express = require('express');
/* FrameWork Packages End*/


/* Configuration Packages Start  */
const dotenv = require('dotenv');
/* Configuration Packages End  */


/* Security  Packages Start  */
const mongoSanitize = require('express-mongo-sanitize');
const helmet = require('helmet')
const  xss = require('xss-clean')
const rateLimit = require("express-rate-limit");
const hpp = require('hpp');
const cors = require('cors')
const cookieParser = require('cookie-parser');
/* Security  Packages End  */

const fileUpload = require('express-fileupload');

/* Application Routes Start */
const book = require('./routes/book.routes');
/* Application Routes End */

const path = require('path');


const connectDB = require('./config/db');
//load config
dotenv.config({ path: './config/config.env' });
const logger = require('./middlewares/logger');
const errorHandler = require('./middlewares/error')

const morgan= require('morgan');
const PORT = process.env.PORT || 5000;
const app = express();
// body parser
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000, // 15 minutes
  max: 100 // limit each IP to 100 requests per windowMs
});


/* Application MiddleWares Start */
app.use(limiter);
app.use(hpp());
app.use(cors())
app.use(express.json());
app.use(cookieParser());
app.use(mongoSanitize());
app.use(helmet())
app.use(xss())
/* Application MiddleWares End */
connectDB();

// app.use(logger);
if(process.env.NODE_ENV=='development'){

  app.use(morgan('dev'))
}
app.use(fileUpload());

// set static folder

app.use(express.static(path.join(__dirname,'public')));
//app.use(logger);
/** */


/* Application Controllers Files Start */

app.use('/api/v1/books', book);

/* Application Controllers Files End */

app.use(errorHandler);
app.get('/', (req, res) => {
	res.send('Hello from express');
});

const server = app.listen(PORT, () => {
	console.log(
		`Server running in ${process.env.NODE_ENV} mode on port ${process.env.PORT}`
	);
});

// handle unhandled promise rejection

process.on('unhandledRejection',(err,promise)=>{
  console.log(`unhandled error ${err}`);
  server.close(()=>process.exit(1));
})
