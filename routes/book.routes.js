/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-08-04T10:22:39+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T12:26:19+05:30
 */



const express = require('express');
const router = express.Router();
const {
	getBooks,
	getBook,
	createBook,
	updateBook,
  deleteBook,
} = require('../controllers/book.controller');


// Include other resource routes
const advancedResults = require('../middlewares/advancedResults');
const BookModel = require('../models/book.model');


router.route('/')
.get(advancedResults(BookModel),getBooks)
.post(createBook);

router.route('/:id')
.get(getBook)
.put(updateBook)
.delete(deleteBook)




module.exports = router;
