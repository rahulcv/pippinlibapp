/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-06-28T17:08:25+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:00:30+05:30
 */



const asyncHandler = fn => (req, res, next) =>
  Promise.resolve(fn(req, res, next)).catch(next);

module.exports = asyncHandler;
