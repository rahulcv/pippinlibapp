/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-06-28T17:08:25+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:00:22+05:30
 */



const logger= (req, res, next)=>{

  req.hello = 'hello world';
  console.log(`${req.method}   ${req.protocol} ${req.originalUrl} `)
  next();

}
module.exports= logger;
