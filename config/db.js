/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-06-28T17:08:25+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:00:46+05:30
 */



const mongoose = require('mongoose');
const connectDB = async () => {
  try{
  //const conn = await mongoose.connect(`mongodb+srv://nurv:r4realiable@swatapi-uagcm.mongodb.net/nurvapi?retryWrites=true&w=majority`,
  const conn = await mongoose.connect(process.env.MONGO_URI,
 {useNewUrlParser:true,
   useCreateIndex:true,
   useFindAndModify:false,
   useUnifiedTopology: true
  }
    );
    console.log(`mongo db connected ${ conn.connection.host}`)

  }catch(e){
    console.log(e)
  }
  }
module.exports = connectDB;
