/**
 * @Author: RahulCv <rahulcv>
 * @Date:   2020-08-04T13:31:40+05:30
 * @Email:  rahulcv@hotmail.co.in
 * @Last modified by:   rahulcv
 * @Last modified time: 2020-08-04T13:56:16+05:30
 */

let chai = require('chai');
let chaiHttp = require('chai-http');
var should = chai.should();
var describe = mocha.describe
var it = mocha.it
var assert = require('chai').assert
chai.use(chaiHttp);
let server = require('./server');
//Our parent block
describe('Podcast', () => {
 describe('/GET Books', () => {
     it('it should GET all the Books', (done) => {
     chai.request(server)
       .get('/books')
       .end((err, res) => {
             (res).should.have.status(200);
             (res.body).should.be.a('object');
             (res.body.podcasts.length).should.be.eql(1);
             done();
          });
       });
  });
describe('/GET Booby id', () => {
     it('it should GET a message', (done) => {
     chai.request(server)
         .get('/book/5f2917c9a3814300d8ada623')
         .end((err, res) => {
               (res).should.have.status(200);
               (res.body).should.be.a('object');
               done();
            });
         });
     });
});
